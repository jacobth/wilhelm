// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { Card, Row, Column, Form, Button } from './widgets';
import authService, { type User } from './task-service';
import CryptoJS from 'crypto-js'


class LogInn extends Component {
  user: User = [];
  page: number = 0
  render() {
    if (this.page == 0) return (
      <Card title="Log Inn">
        <Row>
          <Column width={1}>
            <Form.Label>Username:</Form.Label>
          </Column>
          <Column width={4}>
            <Form.Input
              type="text"
              value={this.usname}
              onChange={(event) => (this.usname = event.currentTarget.value)}
            ></Form.Input>
          </Column>
        </Row>
        <Row>
          <Column width={1}>
            <Form.Label>Password:</Form.Label>
          </Column>
          <Column width={4}>
            <Form.Input
              type="password"
              value={this.pass}
              onChange={(event) => (this.pass = event.currentTarget.value)}
            ></Form.Input>
          </Column>
        </Row>
        <Button.Success
          onClick={() => {
            let resp = this.hash_things(this.pass, this.usname)
            authService.send(resp, this.usname).then((response) => {
              if (response.auth == 1) {
              this.user.usname = response.usname
              this.user.pass_clear = this.pass
              this.user.pass_hashed = response.pass
              this.user.pass_hashed_server = response.pass_server
              this.user.id = response.id
              this.page = 1;
              }
              else if(response.auth == 0){
                console.log(response)
                alert("wrong password for this user")
              } 
              else if(response.auth == -1){
                console.log(response)
                alert("Incorrect username")
              }
            });
          }}
        >
          Log inn
        </Button.Success>
      </Card>
    );
    else if(this.page == 1 ){
      return (
        <Card title="User Info">
          <Row>
            <Column>Username</Column>
            <Column>{this.user.usname}</Column>
          </Row>
          <Row>
            <Column>Password</Column>
            <Column>{this.user.pass_clear}</Column>
          </Row>
          <Row>
            <Column>Hashed password from client</Column>
            <Column>{this.user.pass_hashed}</Column>
          </Row>
          <Row>
            <Column>Hashed password from serverside</Column>
            <Column>{this.user.pass_hashed_server}</Column>
          </Row>
          <Row>
            <Column>Id</Column>
            <Column>{this.user.id}</Column>
          </Row>
      </Card>
      )
    }
  }
  hash_things(pass, usname){
    var key512Bits = CryptoJS.PBKDF2(pass, usname, { keySize: 512/32 });
    console.log(key512Bits.toString())
    return key512Bits.toString()
  }

}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <>
      <LogInn />
    </>,
    root
  );
