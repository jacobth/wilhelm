// @flow
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type User = {
  usname: string,
  pass_clear: string,
  pass_hashed: string,
  pass_hashed_server: string,
  id: number
};

class authService {  
  send(pass: string, usname: string) {
    return axios
      .post<>('/user', { pass: pass, usname: usname })
      .then((response) => 
        response.data);
  }
}

export default new authService();
