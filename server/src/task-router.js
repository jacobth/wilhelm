// @flow
import express from 'express';
import authService from './task-service';


const router: express$Router<> = express.Router();


router.post('/user', (request, response) => {
  if (request.body)
    authService
      .func1(request.body.pass, request.body.usname)
      .then((resp) => {
        response.send(resp)})
      .catch((error: Error) => response.status(400).send(error));
  else response.status(500).send('No params.');
});



export default router;
