// @flow

import CryptoJS from 'crypto-js'



let user_array: array = [];
user_array.push({
  usname: "Ola",
  pass: "aa5fc36a21b7c2e74d066a7ef870dff6e69b7819a563a8c08db3ba492d7713610bee21cb5a2860ef784c399193271617867b2a754364042a22a4b9b5ef0cb437",
})

class authService {
 
  func1(pass: string, usname: string) {
    let server_pass = this.serverHash(pass)
    let i = user_array.findIndex(x => x.usname == usname);
    return new Promise<>((resolve, reject) => {
      if(i  != -1) {
        if(user_array[i].pass == server_pass) resolve({usname: usname, pass:pass, pass_server:server_pass, id: i, auth: 1})
        else(resolve({usname: usname, pass:pass, pass_server: null, id: null, auth: 0}))
      }
      else if (i == -1){
        resolve({usname: usname, pass:pass, pass_server: null, id: null, auth: -1})
      }
    })
  }

  serverHash(pass: string){
    var key512BitsV2 = CryptoJS.PBKDF2(pass, "Some advanced salt", { keySize: 512/32 });
    return key512BitsV2.toString()
  }

}

export default new authService();
